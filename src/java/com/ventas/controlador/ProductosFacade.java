/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ventas.controlador;

import com.ventas.entidades.Productos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Mauro
 */
@Stateless
public class ProductosFacade extends AbstractFacade<Productos> {

    @PersistenceContext(unitName = "empresaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductosFacade() {
        super(Productos.class);
    }
    
    public List<Object[]> productosCantidad(){
        Query q=em.createNativeQuery("SELECT  descripcion_producto as Productos, id_categoria as Categoria FROM productos");
        
        return q.getResultList();
    }
    public List<Object[]> productosProveedor(){
        Query q=em.createNativeQuery("SELECT  count(id_producto) as Productos, id_proveedor as Proveedor FROM productos group by id_proveedor");
        
        return q.getResultList();
    }
    public List<Object[]> productosCategoria(){
        Query q=em.createNativeQuery("SELECT  count(id_producto) as Productos, id_categoria as Categoria FROM productos group by id_categoria");
        
        return q.getResultList();
    }       
    public List<Object[]> productos(){
        Query q=em.createNativeQuery("SELECT id_producto, descripcion_producto, precio_producto, id_categoria,id_proveedor FROM productos;");
        
        return q.getResultList();
    }      
}
