/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ventas.controlador;

import com.ventas.entidades.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Mauro
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "empresaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    public Usuario obtenerUsuarioxNombre(String nombreUsuario){
        
        // en la siguiente consulta hay un Usuario.class, esto se usa para dar forma a los resultados de la consulta
        // y cuando se usa el setparameter el primer campo "xxx" es como aparece en la consulta que esta en usuario.java
        // y el otro valor es como llego el parametro en el metodo
        Query q=em.createNamedQuery("Usuario.findByNombreUsuario", Usuario.class).setParameter("nombreUsuario", nombreUsuario);
        
        List<Usuario> listaUsuarios = q.getResultList();
        
        if(listaUsuarios.size()>0){
            
            return listaUsuarios.get(0);
            
        }else{
            return null;
        }
        
    }
}
