/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ventas.controlador;


import com.javapapers.java.social.facebook.FBConnection;
import com.ventas.entidades.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
// el siguiente import es para MD5 cifrar password
//import org.apache.commons.codec.digest.DigestUtils;


/**
 *
 * @author Mauro
 */
@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {

    /**
     * Creates a new instance of LoginController
     */
    
    @EJB // Esto es lo que hace que se inyecte y se pueda conectar con la BD
    // Usuario para poder conectar a la base de datos
    private UsuarioFacade usuarioFacade; // se deja privado sin hacer Get and Setter, 
    //para evitar que una persona se conecte desde el index a la BD, se llama relación fuerte
       
    private String login;
    private String password;
    private Usuario usuarioLogueado;
    private FBConnection conectionFb=new FBConnection();

    public Usuario getUsuarioLogueado() {
        return usuarioLogueado;
    }

    public void setUsuarioLogueado(Usuario usuarioLogueado) {
        this.usuarioLogueado = usuarioLogueado;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public LoginController() {
    }
    
    /*public String cifrarPassword(String Password){
        passwordCifrado = DigestUtils.md5Hex(password);
        return passwordCifrado;
    }*/
    
    
    public String autenticarUsuario(){  
        /*if (login.equals("mmosquera") && password.equals("123")){     
            return "acceder";
        }*/
        Usuario usuarioEncontrado= usuarioFacade.obtenerUsuarioxNombre(login);
        
        if(usuarioEncontrado!=null){
            
            if(usuarioEncontrado.getPasswordUsuario().equals(password)){
                usuarioLogueado=usuarioEncontrado;
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("username", login);
                return "acceder";
            }else{     
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage
                (FacesMessage.SEVERITY_ERROR,"Password Errado","Password Errado"));
            } 
                
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage
            (FacesMessage.SEVERITY_ERROR,"Nombre No Existe","Nombre No Existe"));
        }
        
        return null;
    }
    
    public String getObtenerConexionFb(){
        
        //FBConnection fbprueba = new FBConnection();
        //FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("username","facebook");
        return conectionFb.getFBAuthUrl();
    }
    public String cerrarSesion(){
        
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        
        return "loginPage";
    }
}
