/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ventas.controlador;

import com.javapapers.java.social.facebook.FBConnection;
import com.javapapers.java.social.facebook.FBGraph;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Mauro
 */
@Named(value = "comunicacionFaceBook")
@RequestScoped
public class ComunicacionFaceBook {

    private String codigo="";
    private String nombre="";
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
    /**
     * Creates a new instance of ComunicacionFaceBook
     */
    public ComunicacionFaceBook() {
    }
    @PostConstruct
    public void analizarCodeFB(){
        
        Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        
        codigo = (String) params.get("code");
        
        		if (codigo == null || codigo.equals("")) {
			throw new RuntimeException(
					"ERROR: Didn't get code parameter in callback.");
		}
		FBConnection fbConnection = new FBConnection();
		String accessToken = fbConnection.getAccessToken(codigo);

		FBGraph fbGraph = new FBGraph(accessToken);
		String graph = fbGraph.getFBGraph();
		Map<String, String> fbProfileData = fbGraph.getGraphData(graph);


		nombre = fbProfileData.get("name");
        try {
            FacesContext.getCurrentInstance().getExternalContext().
                    redirect("http://localhost:8080/empresa/faces/usuario/List.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ComunicacionFaceBook.class.getName()).log(Level.SEVERE, null, ex);
        }
                     

    }
}
