package com.ventas.controlador;

import com.ventas.entidades.Clientes;
import com.ventas.controlador.util.JsfUtil;
import com.ventas.controlador.util.JsfUtil.PersistAction;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.event.map.StateChangeEvent;

import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.LatLngBounds;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;



@Named("clientesController")
@SessionScoped
public class ClientesController implements Serializable {

    @EJB
    private com.ventas.controlador.ClientesFacade ejbFacade;
    private List<Clientes> items = null;
    private Clientes selected;
    private MapModel simpleModel;

    public ClientesController() {
    }

    public Clientes getSelected() {
        return selected;
    }

    public void setSelected(Clientes selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ClientesFacade getFacade() {
        return ejbFacade;
    }

    public Clientes prepareCreate() {
        selected = new Clientes();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ClientesCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ClientesUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ClientesDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Clientes> getItems() {     
        if (items == null) {
              items = getFacade().findAll();
            }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction == PersistAction.DELETE) {
                    getFacade().remove(selected);
                } else {
                    if (persistAction == PersistAction.CREATE){
                        getFacade().create(selected);
                    }else{
                        getFacade().edit(selected);
                    }
                }
                JsfUtil.addSuccessMessage(successMessage);
            }catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Clientes getClientes(java.math.BigDecimal id) {
        return getFacade().find(id);
    }

    public List<Clientes> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Clientes> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Clientes.class)
    public static class ClientesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ClientesController controller = (ClientesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "clientesController");
            return controller.getClientes(getKey(value));
        }

        java.math.BigDecimal getKey(String value) {
            java.math.BigDecimal key;
            key = new java.math.BigDecimal(value);
            return key;
        }

        String getStringKey(java.math.BigDecimal value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Clientes) {
                Clientes o = (Clientes) object;
                return getStringKey(o.getIdCliente());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Clientes.class.getName()});
                return null;
            }
        }

    }
    
    //Este es el código que toma los puntos del mapa
    public void onPointSelect(PointSelectEvent event) {
        LatLng latlng = event.getLatLng();        
        addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "Punto Seleccionado", "Latitud:" + latlng.getLat() + ", Longitud:" + latlng.getLng()));
    }
      
    public void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }        
    
    @PostConstruct
    public void init() {
        simpleModel = new DefaultMapModel();
        
        items = getFacade().findAll();
        
        for(Clientes mauro: items){
            LatLng coord1 = new LatLng(mauro.getFloatlatitud(), mauro.getFloatlongitud());
            simpleModel.addOverlay(new Marker(coord1, mauro.getNombreCliente()));
        }

    }
    
    public MapModel getSimpleModel() {
        return simpleModel;
    }    

}
