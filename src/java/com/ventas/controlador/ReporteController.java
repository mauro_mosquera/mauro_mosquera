/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ventas.controlador;

import javax.inject.Named;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

//Import para el grafico
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author Mauro
 */
@Named(value = "reporteController")
@ViewScoped
public class ReporteController implements Serializable {
    
    @EJB
    private ProductosFacade connProductos;
    private List<Object[]> listaProductosCantidad;
    private List<Object[]> listaProductosProveedor;
    private List<Object[]> listaProductosCategoria;
    private List<Object[]> listaProductos;
    private BarChartModel animatedModel2;
    private PieChartModel pieModel1;
    private HorizontalBarChartModel horizontalBarModel;

    public List<Object[]> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(List<Object[]> listaProductos) {
        this.listaProductos = listaProductos;
    }

    public HorizontalBarChartModel getHorizontalBarModel() {
        return horizontalBarModel;
    }

    public void setHorizontalBarModel(HorizontalBarChartModel horizontalBarModel) {
        this.horizontalBarModel = horizontalBarModel;
    }
    
    
    public List<Object[]> getListaProductosCategoria() {
        return listaProductosCategoria;
    }

    public void setListaProductosCategoria(List<Object[]> listaProductosCategoria) {
        this.listaProductosCategoria = listaProductosCategoria;
    }

    
    public List<Object[]> getListaProductosProveedor() {
        return listaProductosProveedor;
    }

    public void setListaProductosProveedor(List<Object[]> listaProductosProveedor) {
        this.listaProductosProveedor = listaProductosProveedor;
    }
    

    public PieChartModel getPieModel1() {
        return pieModel1;
    }

    public void setPieModel1(PieChartModel pieModel1) {
        this.pieModel1 = pieModel1;
    }
    
    
    public BarChartModel getAnimatedModel2() {
        return animatedModel2;
    }

    public void setAnimatedModel2(BarChartModel animatedModel2) {
        this.animatedModel2 = animatedModel2;
    }
    
    public List<Object[]> getListaProductosPrecios() {
        return listaProductosCantidad;
    }

    public void setListaProductosPrecios(List<Object[]> listaProductosPrecios) {
        this.listaProductosCantidad = listaProductosPrecios;
    }
    
    
    /**
     * Creates a new instance of ReporteController
     */
    public ReporteController() {

    }
    
    @PostConstruct
    public void cargarReporte(){
        listaProductosCantidad=connProductos.productosCantidad();
        listaProductosProveedor=connProductos.productosProveedor();
        listaProductosCategoria=connProductos.productosCategoria();
        listaProductos=connProductos.productos();
        createAnimatedModels();    
        createPieModel1();
        createHorizontalBarModel();
    }
    //Grafico Animado
    private void createAnimatedModels() {       
        animatedModel2 = initBarModel();
        animatedModel2.setTitle("Productos Y Cantidad");
        animatedModel2.setAnimate(true);
        animatedModel2.setLegendPosition("ne");
        Axis yAxis = animatedModel2.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(10);
    }    
    
    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel(); 
        ChartSeries Productos = new ChartSeries();
        Productos.setLabel("Productos VS Cantidad");
        for(Object[] obj: listaProductosCantidad){
            Productos.set(obj[0].toString(), (Number) obj[1]);
        }
        model.addSeries(Productos);         
        return model;
    }
    //Grafico de PIE    
    private void createPieModel1() {
        pieModel1 = new PieChartModel();
        
        for(Object[] obj: listaProductosProveedor){
            pieModel1.set(obj[0].toString(), (Number) obj[1]);
        }        
         
        pieModel1.setTitle("Productos X Provedor");
        pieModel1.setLegendPosition("w");
    }    
    //Grafico Horizontal
    private void createHorizontalBarModel() {
        horizontalBarModel = new HorizontalBarChartModel();
 
        ChartSeries productos = new ChartSeries();
        productos.setLabel("Productos y Categorias");
        
        for(Object[] obj: listaProductosCategoria){
            productos.set(obj[0], (Number) obj[1]);
        }          
 
        horizontalBarModel.addSeries(productos);
         
        horizontalBarModel.setTitle("Productos y Categorias");
        horizontalBarModel.setLegendPosition("e");
        //horizontalBarModel.setStacked(true);
         
        Axis xAxis = horizontalBarModel.getAxis(AxisType.Y);
        xAxis.setLabel("PRODUCTOS");
        xAxis.setMin(0);
        xAxis.setMax(10);
         
        Axis yAxis = horizontalBarModel.getAxis(AxisType.X);
        yAxis.setLabel("CATEGORIAS");        
    }    
}
