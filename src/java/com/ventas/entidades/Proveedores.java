/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ventas.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mauro
 */
@Entity
@Table(name = "proveedores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proveedores.findAll", query = "SELECT p FROM Proveedores p")
    , @NamedQuery(name = "Proveedores.findByIdProveedor", query = "SELECT p FROM Proveedores p WHERE p.idProveedor = :idProveedor")
    , @NamedQuery(name = "Proveedores.findByNombreProvedor", query = "SELECT p FROM Proveedores p WHERE p.nombreProvedor = :nombreProvedor")
    , @NamedQuery(name = "Proveedores.findByDireccionProvedor", query = "SELECT p FROM Proveedores p WHERE p.direccionProvedor = :direccionProvedor")
    , @NamedQuery(name = "Proveedores.findByTelefonoProvedor", query = "SELECT p FROM Proveedores p WHERE p.telefonoProvedor = :telefonoProvedor")})
public class Proveedores implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_proveedor")
    private BigDecimal idProveedor;
    @Size(max = 2147483647)
    @Column(name = "nombre_provedor")
    private String nombreProvedor;
    @Size(max = 2147483647)
    @Column(name = "direccion_provedor")
    private String direccionProvedor;
    @Column(name = "telefono_provedor")
    private BigInteger telefonoProvedor;
    @OneToMany(mappedBy = "idProveedor")
    private List<Productos> productosList;

    public Proveedores() {
    }

    public Proveedores(BigDecimal idProveedor) {
        this.idProveedor = idProveedor;
    }

    public BigDecimal getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(BigDecimal idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getNombreProvedor() {
        return nombreProvedor;
    }

    public void setNombreProvedor(String nombreProvedor) {
        this.nombreProvedor = nombreProvedor;
    }

    public String getDireccionProvedor() {
        return direccionProvedor;
    }

    public void setDireccionProvedor(String direccionProvedor) {
        this.direccionProvedor = direccionProvedor;
    }

    public BigInteger getTelefonoProvedor() {
        return telefonoProvedor;
    }

    public void setTelefonoProvedor(BigInteger telefonoProvedor) {
        this.telefonoProvedor = telefonoProvedor;
    }

    @XmlTransient
    public List<Productos> getProductosList() {
        return productosList;
    }

    public void setProductosList(List<Productos> productosList) {
        this.productosList = productosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProveedor != null ? idProveedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedores)) {
            return false;
        }
        Proveedores other = (Proveedores) object;
        if ((this.idProveedor == null && other.idProveedor != null) || (this.idProveedor != null && !this.idProveedor.equals(other.idProveedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        //return "com.ventas.entidades.Proveedores[ idProveedor=" + idProveedor + " ]";
        return idProveedor + " - " + nombreProvedor;
    }
    
}
